// const os = require("os")
// console.log("Free Memory: ", os.freemem())

// const luasSegitiga = require("./segitiga")
// console.log(luasSegitiga(10, 15))

// const fs = require("fs")
// const isi = fs.readFileSync("./package.json")
// const data = JSON.parse(isi.toString())
// console.log(data.name)

// const fs = require("fs")
// const content = "Hello world"
// fs.writeFileSync("./text.txt", content)

const fs = require("fs")

function createPerson(data) {
    let jsonData = JSON.stringify(data, null, 2)
    fs.writeFileSync("./person.json", jsonData)

    return data
}

const Sabrina = createPerson({
    name: "Sabrina",
    age: 25,
    address: "BSD"
})

console.log(Sabrina)

function readPerson() {
    let data = fs.readFileSync("./person.json")

    return JSON.parse(data.toString())
}

const person = readPerson()
console.log("name:", person.name)
console.log("age:", person.age)
console.log("address:", person.address)

person.name = "John"
person.age = 24
person.address = "Jakarta"
createPerson(person)